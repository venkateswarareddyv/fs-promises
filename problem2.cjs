let fs = require('fs');

let path = require('path')

//Reading lipsum.txt file
function readingLipsiumFile() {
    return new Promise((resolve, reject) => {
        const paths = path.join(__dirname, 'lipsum.txt')
        fs.readFile(paths, 'utf8', (error, data) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    })
}



function convertingAndSortingData(data) {
    return new Promise((resolve, reject) => {
        const upperCaseFilePath = path.join(__dirname, 'lipsumUpper.txt');
        const fileName = path.join(__dirname, 'filenames.txt');

        let dataInUpperCase = data.toUpperCase();

        fs.writeFile(upperCaseFilePath, dataInUpperCase, (error) => {
            if (error) {
                reject("Error occured in writing data to the file");
            } else {
                fs.writeFile(fileName, upperCaseFilePath, (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve("Uppercase.txt created and filenames updated");
                    }
                });
            }
        });
    })
}

function convertTwoLowerCases() {
    return new Promise((resolve, reject) => {
        const upperCaseFilePath = path.join(__dirname, 'lipsumUpper.txt');

        fs.readFile(upperCaseFilePath, 'utf8', (error, data) => {
            if (error) {
                reject("Error occured during reading file");
            } else {
                const lowerCaseFilePath = path.join(__dirname, 'lipsumLower.txt');
                const fileName = path.join(__dirname, 'filenames.txt');

                let dataInLowerCase = data.toLowerCase();
                let dataInSentences = dataInLowerCase.split('.').join('\n');

                fs.writeFile(lowerCaseFilePath, dataInSentences, (error, data) => {
                    if (error) {
                        reject("Error occured in writing data to the file");
                    } else {
                        fs.writeFile(fileName, '\n' + lowerCaseFilePath, { flag: 'a' }, (error) => {
                            if (error) {
                                reject(error)
                            } else {
                                resolve("Lowercase.txt created and filenames updated")
                            }
                        });
                    }
                });
            }
        });
    })
}

function readingBothFilesAndSorting() {
    return new Promise((resolve, reject) => {
        const upperCaseFilePath = path.join(__dirname, 'lipsumUpper.txt');
        const lowerCaseFilePath = path.join(__dirname, 'lipsumLower.txt');

        fs.readFile(upperCaseFilePath, 'utf8', (error, dataInUpper) => {
            if (error) {
                reject("Error occured during reading file");
            } else {
                fs.readFile(lowerCaseFilePath, 'utf8', (error, dataInLower) => {

                    const sortedDataFilePath = path.join(__dirname, 'lipsumSorted.txt');
                    const filesName = path.join(__dirname, 'filenames.txt');

                    let totalData = dataInLower + dataInUpper;
                    let sortedData = totalData.split(' ').sort().join('\n');

                    fs.writeFile(sortedDataFilePath, sortedData, (error) => {
                        if (error) {
                            re("Error occured in writing data to the file");
                        } else {
                            fs.writeFile(filesName, '\n' + sortedDataFilePath, { flag: 'a' }, (error) => {
                                if (error) {
                                    reject(error);
                                } else {
                                    resolve("Reading Both Files,sorting the content,write it in a new file and files name updated");
                                }
                            });
                        }
                    });
                });
            }
        });
    })
}

const readingAndDeletingFiles = () => {
    return new Promise((resolve, reject) => {

        let filesNamePath = path.join(__dirname, 'filenames.txt');
        fs.readFile(filesNamePath, 'utf-8', (err, data) => {
            if (err) {
                reject("Error happend while reading the file");
            }
            else {
                let dataList = data.split('\n');
                dataList.forEach((item) => {
                    fs.unlink(item, (error) => {
                        if (error) {
                            reject("Error happend while deleting the file");
                        }
                    })
                })
                resolve("Files deleted");

            }
        })
    })

}


module.exports = { readingLipsiumFile, convertingAndSortingData, convertTwoLowerCases, readingBothFilesAndSorting,readingAndDeletingFiles};
