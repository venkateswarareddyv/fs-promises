let fs = require('fs');
let path = require('path')

function toCreateDirectory(folderName) {
    return new Promise((resolve, reject) => {
        let paths = path.join(__dirname, folderName)
        fs.mkdir(paths, { recursive: true }, (err) => {
            if (err) {
                console.log("failure")
                reject(err);
            } else {
                resolve()

            }
        })
    })
}

function createRandomJsonFiles(x) {
    return new Promise((resolve, reject) => {
        for (let i = 0; i < x; i++) {
            let paths = path.join(__dirname, "tests")
            fs.writeFile(paths +`/fileName${i}.JSON`,"dummy", (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        }

    })
}

function deleteRandomGeneratedFiles(x){
    return new Promise((resolve,reject)=>{
        for (let i = 0; i < x; i++) {
            let paths = path.join(__dirname, "tests")
            fs.unlink(paths+`/fileName${i}.JSON`,(error)=>{
                if(error){
                    reject(error);
                }else{
                    resolve();
                }
            })
        }
    })
}


module.exports = {toCreateDirectory,createRandomJsonFiles,deleteRandomGeneratedFiles};


