let {readingLipsiumFile,convertingAndSortingData,convertTwoLowerCases,readingBothFilesAndSorting,readingAndDeletingFiles}=require('../problem2.cjs');


readingLipsiumFile()
.then((data)=>{
    return convertingAndSortingData(data);
})
.then(()=>{
    return convertTwoLowerCases();
})
.then(()=>{
    return readingBothFilesAndSorting();
})
.then(()=>{
     return readingAndDeletingFiles();
})
.then((data)=>{
    console.log(data);
})
.catch((error)=>{
    console.log(error);
})